
// AnalysePC3Dlg.cpp : 实现文件
//

#include "stdafx.h"
#include <fstream>//ifstream读文件，ofstream写文件，fstream读写文件
#include <string>//文本对象，储存读取的内容
#include <iostream>//屏幕输出cout
#include <cstdlib>//调用system("pause");
#include <vector>
#include <set>
#include <map>
#include <algorithm>
using namespace std;
#include "AnalysePC3.h"
#include "AnalysePC3Dlg.h"


#include "../zlib-1.2.10/zlib.h"
#include <locale.h>
//#include "LibrePIA.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

extern int read_header(char *infilename);
extern int decompress_data(char *infilename, char *outfilename);
extern int write_PIA(const char *infilename, const char *outfilename);
extern int plot_model_parameter_parser(const char *infilename);

// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CAnalysePC3Dlg 对话框

CAnalysePC3Dlg::CAnalysePC3Dlg(CWnd* pParent /*=NULL*/)
	: CDialog(CAnalysePC3Dlg::IDD, pParent)
	, m_strInfo(_T("PIA:打印配置或打印样式文件."))
	, m_csType(_T(""))
	, m_width(420)
	, m_height(297)
	, m_area(124740)
	, m_pc3Name(_T(""))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CAnalysePC3Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_INFORMATION, m_strInfo);
	DDX_Control(pDX, IDC_COMBO_TYPE, m_cmb_type);
	DDX_CBString(pDX, IDC_COMBO_TYPE, m_csType);
	DDX_Text(pDX, IDC_EDIT_WIDTH, m_width);
	DDX_Text(pDX, IDC_EDIT_HEIGHT, m_height);
	DDV_MinMaxDouble(pDX, m_width, 1, 50000);
	DDV_MinMaxDouble(pDX, m_height, 1, 50000);
	DDX_Control(pDX, IDC_LIST_SIZE, m_list);
	DDX_Text(pDX, IDC_PC3NAME, m_pc3Name);
	DDX_Control(pDX, IDC_ANALYSE, m_btn_PIA2TXT);
	DDX_Control(pDX, IDC_SAVEPIA, m_btn_TXT2PIA);
	DDX_Control(pDX, IDC_OPENPC3, m_btn_open);
}

BEGIN_MESSAGE_MAP(CAnalysePC3Dlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_ANALYSE, &CAnalysePC3Dlg::OnBnClickedAnalyse)
	ON_BN_CLICKED(IDC_OPENPC3, &CAnalysePC3Dlg::OnBnClickedOpenPC3)
	ON_BN_CLICKED(IDC_SAVEPIA, &CAnalysePC3Dlg::OnBnClickedSavePIA)
	ON_BN_CLICKED(IDC_BTN_ADD, &CAnalysePC3Dlg::OnBnClickedBtnAdd)
	ON_BN_CLICKED(IDC_BTN_DEL, &CAnalysePC3Dlg::OnBnClickedBtnDel)
	ON_BN_CLICKED(IDC_BTN_MAKE, &CAnalysePC3Dlg::OnBnClickedBtnMake)
	ON_WM_DROPFILES()
END_MESSAGE_MAP()


// CAnalysePC3Dlg 消息处理程序

BOOL CAnalysePC3Dlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码
	m_cmb_type.SetCurSel(0);
	m_cmb_type.GetLBText(0,m_csType);
	initList();
	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CAnalysePC3Dlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CAnalysePC3Dlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CAnalysePC3Dlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CAnalysePC3Dlg::initList()
{
	CRect rect;
    m_list.GetWindowRect(&rect);
	ScreenToClient(&rect);
	int width = rect.right - rect.left;
	m_list.InsertColumn(0, _T("序号"), LVCFMT_CENTER, width/6, -1);// 插入列 
	m_list.InsertColumn(1, _T("纸张宽"), LVCFMT_CENTER, width/4, -1);// 插入列 
	m_list.InsertColumn(2, _T("纸张高"), LVCFMT_CENTER, width/4, -1);
	m_list.InsertColumn(3, _T("纸张面积"), LVCFMT_CENTER, width/3, -1);
	m_list.SetExtendedStyle(m_list.GetExtendedStyle()|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);
}

//多选文件
int CAnalysePC3Dlg::getFiles(CString strFilter,vector<std::string> & files)
{
	files.clear();
	CFileDialog fpDlg(TRUE, NULL, NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT | OFN_ALLOWMULTISELECT, strFilter, NULL); //

	DWORD MAXFILE = 40000;//初始化空间大小，若超过初始化空间，程序fd.DoModal()会自己返回IDCANCEL值  
	fpDlg.m_ofn.nMaxFile = MAXFILE;
	TCHAR * pc = new TCHAR[MAXFILE];//定义为T类型，可避免转换  
	USES_CONVERSION;
	fpDlg.m_ofn.lpstrFile = pc;
	fpDlg.m_ofn.lpstrFile[0] = NULL;//对文件选择对话框初始化

	std::string strImgPath;
	if (IDOK == fpDlg.DoModal())
	{
		POSITION pos = fpDlg.GetStartPosition();
		while (pos)
		{
			CString imgPath = fpDlg.GetNextPathName(pos);
			strImgPath = CW2A(imgPath.GetString());
			files.push_back(strImgPath);
		}
	}
	delete[] pc;
	pc = NULL;
	return files.size();
}

void CAnalysePC3Dlg::analyse()
{
	m_out.clear();
	for (size_t i = 0; i < m_pathImage.size(); i++)
	{
		std::string fileIn = m_pathImage[i].c_str();
		std::string fileOut = fileIn.substr(0, fileIn.size() - 4);
		fileOut.append(".txt");
		int ret = 0;
		ret = read_header((char*)fileIn.c_str());
		ret = decompress_data((char*)fileIn.c_str(), (char*)fileOut.c_str());
		m_out.push_back(fileOut);
	}
	m_strInfo = _T("解码完成！");
	OnBnClickedOpenfolder();
	UpdateData(FALSE);
}

void CAnalysePC3Dlg::savePIA()
{
	m_out.clear();
	for (size_t i = 0; i < m_txtFiles.size(); i++)
	{
		std::string strfile = m_txtFiles[i];
		std::string fileCopy = strfile.substr(0, strfile.size() - 4);
		fileCopy.append(".Copy.");
		fileCopy.append(CW2A(m_csType));
		int ret = write_PIA(strfile.c_str(),fileCopy.c_str());
		m_out.push_back(fileCopy);
		//ret = plot_model_parameter_parser(fileOut.c_str());
	}

	m_strInfo = _T("转换完成！");
	OnBnClickedOpenfolder();
	UpdateData(FALSE);
}

void CAnalysePC3Dlg::openPC3()
{
	if (m_pathImage.empty()) 
	{
		m_pc3Name = _T("没选择文件！");
	}
	else
	{
		CString strFileName = CA2W(m_pathImage[0].c_str());
		TCHAR szDrive[_MAX_DRIVE]={0};
		TCHAR szDir[_MAX_DIR]={0};
		TCHAR szFilename[_MAX_FNAME]={0};
		TCHAR szExt[_MAX_EXT]={0};
		_tsplitpath(strFileName,szDrive,szDir,szFilename,szExt);
		m_pc3Name.Format(_T("%s%s"),szFilename,szExt);
	}
	UpdateData(FALSE);
}

//解码PIA文件
void CAnalysePC3Dlg::OnBnClickedAnalyse()
{
	m_pathImage.clear();
	CString strFilter = _T("绘图仪配置文件(*.pc3;*.pmp)|*.pc3;*.pmp|打印样式文件(*.stb;*.ctb)|*.stb;*.ctb|所有文件(*.*)|*.*||");

	int n = getFiles(strFilter,m_pathImage);
	if (m_pathImage.empty()) 
	{
		m_strInfo = _T("没选择文件！");
		UpdateData(FALSE);
		return;
	}
	analyse();
}

//对txt文件压制成PIA文件
void CAnalysePC3Dlg::OnBnClickedSavePIA()
{
	UpdateData(TRUE);
	m_txtFiles.clear();
	int n = getFiles(_T("已解压文件(*.txt)|*.txt|所有文件(*.*)|*.*||"),m_txtFiles);
	if (m_txtFiles.empty()) 
	{
		m_strInfo = _T("没选择文件！");
		UpdateData(FALSE);
		return;
	}
	savePIA();
}

//PIA文件操作后打开其文件夹
void CAnalysePC3Dlg::OnBnClickedOpenPC3()
{
	UpdateData(TRUE);
	m_pathImage.clear();
	CString strFilter = _T("绘图仪配置文件(*.pc3;*.pmp)|*.pc3;*.pmp||");
	int n = getFiles(strFilter,m_pathImage);
	openPC3();
}

//PIA文件操作后打开其文件夹
void CAnalysePC3Dlg::OnBnClickedOpenfolder()
{
	if (m_out.empty()) return;
	CString strFileName = CA2W(m_out[0].c_str());
	TCHAR szDrive[4]={0};
	TCHAR szDir[MAX_PATH] = {0};
	TCHAR szFileName[MAX_PATH] = {0};
	TCHAR szExt[MAX_PATH] = {0};
	_tsplitpath(strFileName,szDrive,szDir,szFileName,szExt);
	CString strPath = szDrive;
	strPath += szDir;
	CString strOut = CA2W(m_out[0].c_str());
	ShellExecute(GetSafeHwnd(),_T("open"), strPath, NULL, strPath,SW_SHOWNORMAL); 
	ShellExecute(GetSafeHwnd(),_T("open"), strFileName, NULL, strPath,SW_SHOWNORMAL); 
}

//增加打印尺寸
void CAnalysePC3Dlg::OnBnClickedBtnAdd()
{
	UpdateData(TRUE);
	m_area = m_height*m_width;
	int cnt = m_list.GetItemCount();
	CString strWidth,strHeight,strArea,strItem,strIndex;
	strIndex.Format(_T("%d"),cnt+1);
	strWidth.Format(_T("%.2f"),m_width);
	strHeight.Format(_T("%.2f"),m_height);
	strArea.Format(_T("%.2f"),m_area);
	strItem.Format(_T("%sx%s"),strWidth,strHeight);

	PAPERINFO paper(m_width,m_height,cnt);
	m_papers.push_back(paper);
	
	m_list.InsertItem(cnt, strItem);				//插入行
	m_list.SetItemText(cnt, 0, strIndex);			//序号
	m_list.SetItemText(cnt, 1, strWidth);			//宽
	m_list.SetItemText(cnt, 2, strHeight);			//高
	m_list.SetItemText(cnt, 3, strArea);			//面积
}

void CAnalysePC3Dlg::OnBnClickedBtnDel()
{
	int count = m_list.GetSelectedCount();
	if (count==0) return;

	std::vector<int> selItems(count);
	int i = 0;
	POSITION  pos = m_list.GetFirstSelectedItemPosition();
	while(pos)
	{
		int k = m_list.GetNextSelectedItem(pos);
		selItems[i++] = k;
	}
	for (std::vector<int>::reverse_iterator it = selItems.rbegin(); it != selItems.rend();++it)
	{
		m_list.DeleteItem(*it);
		m_papers.erase(m_papers.begin()+(*it));
	}

	CString strIndex;
	count = m_list.GetItemCount();
	for (int i = 0; i<count; ++i)
	{
	    strIndex.Format(_T("%d"),i+1);
		m_list.SetItemText(i,0,strIndex);
		m_papers[i].index = i;
	}
}


void CAnalysePC3Dlg::OnBnClickedBtnMake()
{
	if (m_pathImage.empty()||m_papers.empty())
	{
		AfxMessageBox(_T("\n你还没选中PC3文件或者没添加纸张!"));
		return;
	}
	for (size_t i = 0; i < m_pathImage.size(); i++)
	{
		std::string fileIn = m_pathImage[i].c_str();
		std::string fileOut = fileIn.substr(0, fileIn.size() - 4);
		fileOut.append(".txt");
		int ret = read_header((char*)fileIn.c_str());
		ret = decompress_data((char*)fileIn.c_str(), (char*)fileOut.c_str());
		createPMPFile(fileOut,m_papers);
		DeleteFile(CA2W(fileOut.c_str()));
	}
}

void CAnalysePC3Dlg::addData(vector<std::string> &vecData, const PAPERINFO& paper)
{
	double w = paper.width;
	double h = paper.height;
	double s = paper.area;
	CString strDir=(w>h)?_T("横向"):_T("纵向");
	CString str0,str1,str2,str3,str4,str5,str6,str7,str8,str9,strI;
	string s0,s1,s2,s3,s4,s5,s6,s7,s8,s9,si,sd;
	sd = (w>h)? "    landscape_mode=TRUE":"    landscape_mode=FALSE"; 

	str0.Format(_T("UserDefinedMetric %s %.2fW x %.2fH - (0, 0) x (%.2f, %.2f)=%.2f 毫米"),strDir,w,h,w,h,s);
	str1.Format(_T("    name=\"UserDefinedMetric (%.2f x %.2f毫米)"),w,h);
	str2.Format(_T("    localized_name=\"用户%d-%dx%d"),paper.index+1,(int)w,(int)h);
	str3.Format(_T("    media_description_name=\"%s"),str0);
	str4.Format(_T("    name=\"%s"),str0);
	str5.Format(_T("    media_bounds_urx=%.2f"),w);
	str6.Format(_T("    media_bounds_ury=%.2f"),h);
	str7.Format(_T("    printable_bounds_urx=%.2f"),w);
	str8.Format(_T("    printable_bounds_ury=%.2f"),h);
	str9.Format(_T("    printable_area=%.2f"),w);
	strI.Format(_T("    %d{"),paper.index);
	s1 = GbkToUtf8(CW2A(str1.GetString()));
	s2 = GbkToUtf8(CW2A(str2.GetString()));
	s3 = GbkToUtf8(CW2A(str3.GetString()));
	s4 = GbkToUtf8(CW2A(str4.GetString()));
	s5 = CW2A(str5.GetString());
	s6 = CW2A(str6.GetString());
	s7 = CW2A(str7.GetString());
	s8 = CW2A(str8.GetString());
	s9 = CW2A(str9.GetString());
	si = CW2A(strI.GetString());

	vecData.push_back("  size{");
	vecData.push_back(si);
	vecData.push_back("    caps_type=2");
	vecData.push_back(s1);
	vecData.push_back(s2);
	vecData.push_back(s3);
	vecData.push_back("    media_group=15");
	vecData.push_back(sd);
	vecData.push_back("   }");
	vecData.push_back("  }");
	vecData.push_back("  description{");
	vecData.push_back(si);
	vecData.push_back("    caps_type=2");
	vecData.push_back(s4);
	vecData.push_back(s5);
	vecData.push_back(s6);
	vecData.push_back("    printable_bounds_llx=0.0");
	vecData.push_back("    printable_bounds_lly=0.0");
	vecData.push_back(s7);
	vecData.push_back(s8);
	vecData.push_back(s9);
	vecData.push_back("    dimensional=TRUE");
	vecData.push_back("   }");
	vecData.push_back("  }");
}

void CAnalysePC3Dlg::createPMPFile(std::string strFile,const vector<PAPERINFO> & papers)
{
	//首先分离文件名
	CString csFile = CA2W(strFile.c_str());
	CString strPMPFile,strPMPTextFile;
	TCHAR szDrive[_MAX_DRIVE]={0};
	TCHAR szDir[_MAX_DIR]={0};
	TCHAR szFilename[_MAX_FNAME]={0};
	TCHAR szExt[_MAX_EXT]={0};
	_tsplitpath(csFile,szDrive,szDir,szFilename,szExt);
	strPMPFile.Format(_T("%s%sPMP Files\\%s.pmp"),szDrive,szDir,szFilename);
	strPMPTextFile.Format(_T("%s%sPMP Files\\%s.pmp.txt"),szDrive,szDir,szFilename);

	//读取pc3文件的编码
	FILE *infile = fopen(strFile.c_str(), "rb");
	fseek(infile,0,SEEK_END);
	int nBuffer = ftell(infile)+1;
	int num_read = 0;
	char* data = new char[nBuffer];
	data[nBuffer-1]=0;
	fseek(infile,0,SEEK_SET);
	num_read = fread(data, sizeof(char), nBuffer, infile);
	fclose(infile);

	//vector<string> vecData;
	//Split(data, nBuffer, vecData, '\n');

	//找到第一段
	int index = 0;
	for (; index<nBuffer; ++index)
	{
		char c = data[index];
		if (c == '}')
		{
			++index;
			break;
		}
	}

	vector<std::string> vecData;
	vecData.push_back("mod{");
	vecData.push_back(" media{");
	//vecData.push_back("  abilities=\"500005500500505555000005550000000550000500000500000");
	//vecData.push_back("  caps_state=\"000000000000000000000000000000000000000000000000000");
	//vecData.push_back("  ui_owner=\"11111111111111111111110");
	vecData.push_back("  size_max_x=320000.00000");
	vecData.push_back("  size_max_y=320000.00000");
	vecData.push_back(" }");
	vecData.push_back("}");
	//删除段
	vecData.push_back("del{");
	vecData.push_back(" media{");
	//vecData.push_back("  abilities=\"500005500500505555000005550000000550000500000500000");
	//vecData.push_back("  caps_state=\"000000000000000000000000000000000000000000000000000");
	//vecData.push_back("  ui_owner=\"11111111111111111111110");
	vecData.push_back("  size_max_x=320000.00000");
	vecData.push_back("  size_max_y=320000.00000");
	vecData.push_back(" }");
	vecData.push_back("}");
	//用户定义
	vecData.push_back("udm{");
	vecData.push_back(" calibration{");
	vecData.push_back("  _x=1.0");
	vecData.push_back("  _y=1.0");
	vecData.push_back(" }");
	vecData.push_back(" media{");
	//vecData.push_back("  abilities=\"500005500500505555000005550000000550000500000500000");
	//vecData.push_back("  caps_state=\"000000000000000000000000000000000000000000000000000");
	//vecData.push_back("  ui_owner=\"11111111111111111111110");
	vecData.push_back("  size_max_x=320000.00000");
	vecData.push_back("  size_max_y=320000.00000");
	//此段增加数据： 
	for (size_t i= 0; i<papers.size();++i)
	{
		addData(vecData,papers[i]);
	}

	vecData.push_back(" }");
	vecData.push_back("}");
	//隐藏段
	vecData.push_back("hidden{");
	vecData.push_back(" media{");
	//vecData.push_back("  abilities=\"500005500500505555000005550000000550000500000500000");
	//vecData.push_back("  caps_state=\"000000000000000000000000000000000000000000000000000");
	//vecData.push_back("  ui_owner=\"11111111111111111111110");
	vecData.push_back("  size_max_x=320000.00000");
	vecData.push_back("  size_max_y=320000.00000");
	vecData.push_back(" }");
	vecData.push_back("}");
	//写文件
	FILE * outfile = NULL;
	errno_t err = _tfopen_s(&outfile,strPMPTextFile,_T("wb"));
	fwrite(data,sizeof(char),index+1,outfile);
	for (size_t i = 0; i<vecData.size(); ++i)
	{
		string strTemp = vecData[i]+'\n';
		fwrite(strTemp.c_str(),sizeof(char),strTemp.length(),outfile);
	}

	fclose(outfile);
	delete [] data;
	write_PIA(CW2A(strPMPTextFile),CW2A(strPMPFile));
	DeleteFile(strPMPTextFile); //删除文本文件
}

//=======================================================================
//根据分割字符得到字符串组
//=======================================================================
std::vector<CString> CAnalysePC3Dlg::Split(LPCTSTR sRead,TCHAR szSplit /*= '\n'*/)
{
	CString str;
	int i = 0;
	vector<CString> v;
	while (AfxExtractSubString(str,sRead,i,szSplit))
	{
		i++;
		if (!str.IsEmpty())
		{
			v.push_back(str);
		}
	}
	if (v.size()==0)
	{
		v.push_back(sRead);
	}
	return v;
}

int CAnalysePC3Dlg::Split(const char* str,int n,vector<string> & vecRes, char delims)
{
	vecRes.clear();
	std::string strTemp;
	int j = 0;
	for (int i = 0; i< n; ++i)
	{
		if (str[i] == delims)
		{
			int cnt = i-j+1;
			char* s = new char[cnt];
			s[cnt-1]=0;
			memcpy(s,str+j,cnt-1);
			vecRes.push_back(s);
			delete[] s;
			j = i+1;
		}
	}
	if (j < n-1)
	{
		int cnt = n-j+1;
		char* s = new char[cnt];
		s[cnt-1]=0;
		memcpy(s,str+j,cnt-1);
		vecRes.push_back(s);
		delete[] s;
	}
	return vecRes.size();
}

string GbkToUtf8(const char *src_str)
{
	int len = MultiByteToWideChar(CP_ACP, 0, src_str, -1, NULL, 0);
	wchar_t* wstr = new wchar_t[len + 1];
	memset(wstr, 0, len + 1);
	MultiByteToWideChar(CP_ACP, 0, src_str, -1, wstr, len);
	len = WideCharToMultiByte(CP_UTF8, 0, wstr, -1, NULL, 0, NULL, NULL);
	char* str = new char[len + 1];
	memset(str, 0, len + 1);
	WideCharToMultiByte(CP_UTF8, 0, wstr, -1, str, len, NULL, NULL);
	string strTemp = str;
	if (wstr) delete[] wstr;
	if (str) delete[] str;
	return strTemp;
}

string Utf8ToGbk(const char *src_str)
{
	int len = MultiByteToWideChar(CP_UTF8, 0, src_str, -1, NULL, 0);
	wchar_t* wszGBK = new wchar_t[len + 1];
	memset(wszGBK, 0, len * 2 + 2);
	MultiByteToWideChar(CP_UTF8, 0, src_str, -1, wszGBK, len);
	len = WideCharToMultiByte(CP_ACP, 0, wszGBK, -1, NULL, 0, NULL, NULL);
	char* szGBK = new char[len + 1];
	memset(szGBK, 0, len + 1);
	WideCharToMultiByte(CP_ACP, 0, wszGBK, -1, szGBK, len, NULL, NULL);
	string strTemp(szGBK);
	if (wszGBK) delete[] wszGBK;
	if (szGBK) delete[] szGBK;
	return strTemp;
}

void CAnalysePC3Dlg::OnDropFiles(HDROP hDropInfo)
{
	vector<std::string> fileIn;
	int count_droppedfile = DragQueryFile(hDropInfo, 0xFFFFFFFF, NULL, 0);
	for (int i = 0; i < count_droppedfile; ++i)
	{
		wchar_t filepath[MAX_PATH] = { 0 };
		if (DragQueryFile(hDropInfo, i, filepath, MAX_PATH)>0)
		{
			std::string str = CW2A(filepath);
			fileIn.push_back(str);
		}
	}
	POINT pt;
	DragQueryPoint(hDropInfo, &pt);
	DragFinish(hDropInfo);

	CWnd *pWnd = ChildWindowFromPoint(pt);
	if (GetDlgItem(IDC_ANALYSE) == pWnd)
	{
		m_pathImage = fileIn;
		analyse();
	}
	else if (GetDlgItem(IDC_SAVEPIA) == pWnd)
	{
		m_txtFiles = fileIn;
		savePIA();
	}
	else if (GetDlgItem(IDC_OPENPC3) == pWnd)
	{
		m_pathImage = fileIn;
		openPC3();
	}

	CDialog::OnDropFiles(hDropInfo);
}
