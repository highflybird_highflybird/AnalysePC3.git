
// AnalysePC3Dlg.h : 头文件
//

#pragma once
#include "afxwin.h"
#include "afxcmn.h"

struct PAPERINFO
{
	int index;
	double width;
	double height;
	double area;
	CString strWidth;
	CString strHeight;
	CString strArea;
	PAPERINFO()
		:width(1)
		,height(1)
		,area(1)
		,strWidth(_T("1.0"))
		,strHeight(_T("1.0"))
		,strArea(_T("1.0"))
		,index(0)
	{}

	PAPERINFO(double w,double h,int i = 0)
		:width(w)
		,height(h)
		,index(i)
	{
		area = width*height;
		strWidth.Format(_T("%.2f"),width);
		strHeight.Format(_T("%.2f"),height);
		strArea.Format(_T("%.2f"),area);
	}
};

// CAnalysePC3Dlg 对话框
class CAnalysePC3Dlg : public CDialog
{
// 构造
public:
	CAnalysePC3Dlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
	enum { IDD = IDD_ANALYSEPC3_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持

// 实现
protected:
	HICON						m_hIcon;		//图标
	double						m_width;		//编辑框中的宽
	double						m_height;		//编辑框中的高
	double						m_area;			//面积
	CString                     m_pc3Name;		//PC3文件名
	CString						m_strInfo;      //静态控件文本
	CString						m_csType;		//PIA文件类型
	std::vector<PAPERINFO>      m_papers;       //所有增加的纸张
	std::vector<std::string>	m_pathImage;    //选中的文件
	std::vector<std::string>	m_txtFiles;     //选中的文件
	std::vector<std::string>    m_out;          //输出的文件
	CComboBox					m_cmb_type;     //文件类型下拉列表控件
	CListCtrl					m_list;         //纸张列表
	CButton						m_btn_PIA2TXT;
	CButton						m_btn_TXT2PIA;
	CButton						m_btn_open;
	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg void OnBnClickedAnalyse();
	afx_msg void OnBnClickedOpenfolder();
	afx_msg void OnBnClickedOpenPC3();
	afx_msg void OnBnClickedBtnAdd();
	afx_msg void OnBnClickedSavePIA();
	afx_msg void OnBnClickedBtnDel();
	afx_msg void OnBnClickedBtnMake();
	afx_msg void OnDropFiles(HDROP hDropInfo);
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	void initList();
	void DragAcceptFiles( BOOL bAccept = TRUE );
	void analyse();
	void savePIA();
	void openPC3();
	static void addData(vector<std::string> &vecData, const PAPERINFO& paper);
	static void createPMPFile(std::string strFile,const vector<PAPERINFO> & papers);
	static int getFiles(CString strFilter,vector<std::string> & files);
	static int Split(const char* str, int n,vector<string> & vecRes, char delims);
	static std::vector<CString> Split(LPCTSTR sRead,TCHAR szSplit = '\n');
};
string GbkToUtf8(const char *src_str);
string Utf8ToGbk(const char *src_str);