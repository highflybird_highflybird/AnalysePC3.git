//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by AnalysePC3.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_ANALYSEPC3_DIALOG           102
#define IDR_MAINFRAME                   128
#define IDC_ANALYSE                     1000
#define IDC_INFO                        1001
#define IDC_INFORMATION                 1001
#define IDC_BUTTON1                     1002
#define IDC_OPENFOLDER                  1002
#define IDC_OPENPC3                     1002
#define IDC_SAVEPIA                     1003
#define IDC_COMBO_TYPE                  1004
#define IDC_EDIT_WIDTH                  1005
#define IDC_EDIT_HEIGHT                 1006
#define IDC_INFORMATION2                1007
#define IDC_INFORMATION3                1008
#define IDC_BTN_ADD                     1009
#define IDC_LIST_SIZE                   1010
#define IDC_BTN_DEL                     1011
#define IDC_INFORMATION5                1014
#define IDC_PC3NAME                     1014
#define IDC_BTN_MAKE                    1015

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1011
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
